class EnvironmentConfig {
  static const String API_URL = String.fromEnvironment(
    'API_URL',
    defaultValue: 'https://api.exchangeratesapi.io',
  );
}
