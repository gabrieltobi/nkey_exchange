import 'package:dio/dio.dart';
import 'package:dio/native_imp.dart';
import 'package:dio_http_cache/dio_http_cache.dart';
import 'package:nkey_exchange/app/utils/environment_config.dart';

class DioNkey extends DioForNative {
  DioNkey([BaseOptions options]) : super(options) {
    this.options.baseUrl = options?.baseUrl ?? EnvironmentConfig.API_URL;

    this.interceptors.add(DioCacheManager(
          CacheConfig(
            baseUrl: this.options.baseUrl,
            defaultMaxAge: Duration(minutes: 5),
            defaultMaxStale: Duration(days: 1),
          ),
        ).interceptor);
  }
}
