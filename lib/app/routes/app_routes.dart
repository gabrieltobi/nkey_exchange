import 'package:get/get.dart';
import 'package:nkey_exchange/app/modules/exchange/exchange_page.dart';

class MyRoutes {
  static final routes = [
    GetPage(name: '/', page: () => ExchangePage()),
  ];
}
