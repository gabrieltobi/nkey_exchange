import 'dart:math';
import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:nkey_exchange/app/data/providers/exchange_provider.dart';
import 'package:nkey_exchange/app/data/repositories/exchange_repository.dart';
import 'package:nkey_exchange/app/modules/exchange/exchange_controller.dart';
import 'package:nkey_exchange/app/utils/dio_nkey.dart';
import 'package:nkey_exchange/app/widgets/app_bg.dart';

class ExchangePage extends GetView<ExchangeController> {
  static final ExchangeRepository repository = ExchangeRepository(
    apiClient: ExchangeApiClient(httpClient: DioNkey()),
  );
  final ExchangeController _ =
      Get.put(ExchangeController(repository: repository));

  DropdownMenuItem<String> _renderCurrencyItem(String item) {
    return DropdownMenuItem<String>(
      value: item,
      child: Center(
        child: Text(item),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: GestureDetector(
        onTap: () {
          FocusScopeNode currentFocus = FocusScope.of(context);

          if (!currentFocus.hasPrimaryFocus) {
            currentFocus.unfocus();
          }
        },
        child: AppBg(
          child: Obx(
            () => Form(
              key: _.formKey,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Image(
                    image: AssetImage('lib/assets/images/logo.png'),
                    width: 100,
                  ),
                  SizedBox(height: 40),
                  Container(
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(10),
                      color: Colors.white.withOpacity(0.6),
                    ),
                    margin: EdgeInsets.symmetric(horizontal: 10),
                    padding: EdgeInsets.all(10),
                    child: Row(
                      children: [
                        Expanded(
                          child: SizedBox(
                            height: 70,
                            child: Container(
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.horizontal(
                                  left: Radius.circular(5),
                                ),
                                color: Colors.white.withOpacity(0.6),
                              ),
                              child: Center(
                                child: TextFormField(
                                  controller: _.valueController,
                                  keyboardType: TextInputType.number,
                                  textAlign: TextAlign.center,
                                  style: TextStyle(
                                    fontSize: 30,
                                  ),
                                  decoration: InputDecoration(
                                    border: InputBorder.none,
                                  ),
                                ),
                              ),
                            ),
                          ),
                        ),
                        SizedBox(
                          width: 80,
                          child: Container(
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.horizontal(
                                right: Radius.circular(5),
                              ),
                              color: Colors.grey.withOpacity(0.5),
                            ),
                            child: DropdownButton<String>(
                              itemHeight: 70,
                              isExpanded: true,
                              underline: Container(),
                              value: (_.currencyFrom.isNotEmpty
                                  ? _.currencyFrom
                                  : null),
                              onChanged: _.setCurrencyFrom,
                              items: _.currencyFromList
                                  .map(_renderCurrencyItem)
                                  .toList(),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                  SizedBox(height: 10),
                  Container(
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(10),
                      color: Colors.white.withOpacity(0.6),
                    ),
                    margin: EdgeInsets.symmetric(horizontal: 10),
                    padding: EdgeInsets.all(10),
                    child: Column(
                      children: [
                        Row(
                          children: [
                            Expanded(
                              child: SizedBox(
                                height: 70,
                                child: Container(
                                  decoration: BoxDecoration(
                                    borderRadius: BorderRadius.horizontal(
                                      left: Radius.circular(5),
                                    ),
                                    color: Colors.grey.withOpacity(0.2),
                                  ),
                                  child: Center(
                                    child: Text(
                                      _.formatCurrency.format(_.finalValue),
                                      textAlign: TextAlign.center,
                                      style: TextStyle(fontSize: 30),
                                    ),
                                  ),
                                ),
                              ),
                            ),
                            SizedBox(
                              width: 80,
                              child: Container(
                                decoration: BoxDecoration(
                                  borderRadius: BorderRadius.horizontal(
                                    right: Radius.circular(5),
                                  ),
                                  color: Colors.grey.withOpacity(0.5),
                                ),
                                child: DropdownButton<String>(
                                  itemHeight: 70,
                                  isExpanded: true,
                                  underline: Container(),
                                  value: (_.currencyTo.isNotEmpty
                                      ? _.currencyTo
                                      : null),
                                  onChanged: _.setCurrencyTo,
                                  items: _.currencyToList
                                      .map(_renderCurrencyItem)
                                      .toList(),
                                ),
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
                  ),
                  ((_.lastUpdate != '') ? Padding(
                    padding: EdgeInsets.only(top: 10),
                    child: Text(
                      'Última atualização: ${_.lastUpdate}',
                      style: TextStyle(color: Colors.white),
                    ),
                  ) : Container()),
                  Padding(
                    padding: EdgeInsets.symmetric(vertical: 14),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        RawMaterialButton(
                          onPressed: _.switchCurrencies,
                          elevation: 2.0,
                          fillColor: Colors.white,
                          child: Transform.rotate(
                            angle: 90 * (pi / 180),
                            child: Icon(
                              Icons.compare_arrows,
                              size: 30.0,
                            ),
                          ),
                          padding: EdgeInsets.all(12.0),
                          shape: CircleBorder(),
                        ),
                        RawMaterialButton(
                          onPressed: _.getRate,
                          elevation: 2.0,
                          fillColor: Colors.white,
                          child: Icon(
                            Icons.refresh,
                            size: 30.0,
                          ),
                          padding: EdgeInsets.all(12.0),
                          shape: CircleBorder(),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
