import 'package:flutter/widgets.dart';
import 'package:flutter_masked_text/flutter_masked_text.dart';
import 'package:get/get.dart';
import 'package:meta/meta.dart';
import 'package:intl/intl.dart';
import 'package:nkey_exchange/app/data/repositories/exchange_repository.dart';

class ExchangeController extends GetxController {
  final ExchangeRepository repository;
  ExchangeController({@required this.repository}) : assert(repository != null);

  final GlobalKey<FormState> formKey = GlobalKey();
  final formatCurrency = NumberFormat("#,##0.00", "pt_BR");
  final valueController = MoneyMaskedTextController();
  final currencyFromList = List<String>().obs;
  final currencyToList = List<String>().obs;

  final _currencyFrom = 'USD'.obs;
  String get currencyFrom => this._currencyFrom.value;
  set currencyFrom(String value) => this._currencyFrom.value = value;

  final _currencyTo = 'BRL'.obs;
  String get currencyTo => this._currencyTo.value;
  set currencyTo(String value) => this._currencyTo.value = value;

  final _finalValue = 0.0.obs;
  get finalValue => this._finalValue.value;
  set finalValue(value) => this._finalValue.value = value;

  final _loading = false.obs;
  get loading => this._loading.value;
  set loading(value) => this._loading.value = value;

  final _lastUpdate = ''.obs;
  get lastUpdate => this._lastUpdate.value;
  set lastUpdate(value) => this._lastUpdate.value = value;

  void setCurrencyFrom(String newCurrency) {
    currencyFrom = newCurrency;
    getRate();
  }

  void setCurrencyTo(String newCurrency) {
    currencyTo = newCurrency;
    getRate();
  }

  void switchCurrencies() {
    String temp = currencyFrom;
    currencyFrom = currencyTo;
    currencyTo = temp;
    getRate();
  }

  void getRate() async {
    double numericRate = await repository.getRate(currencyFrom, currencyTo);
    finalValue = numericRate * valueController.numberValue;
    lastUpdate = DateFormat('dd/MM/yyyy hh:mm:ss').format(DateTime.now());
  }

  _getCurrencies() async {
    try {
      List<String> currencies = await repository.getCurrencies();
      currencyFromList.addAll(currencies);
      currencyToList.addAll(currencies);
    } catch (e) {
      Get.rawSnackbar(title: 'Ops!', message: e);
    }
  }

  _setupValueChange() {
    valueController.afterChange = (String masked, double raw) {
      getRate();
    };
  }

  @override
  void onInit() {
    _getCurrencies();
    _setupValueChange();
    super.onInit();
  }
}
