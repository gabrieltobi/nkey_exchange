import 'dart:ui';

import 'package:flutter/material.dart';

class AppBg extends StatelessWidget {
  final Widget child;

  const AppBg({this.child});

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        image: DecorationImage(
          image: AssetImage('lib/assets/images/background.png'),
          fit: BoxFit.cover,
        ),
      ),
      child: BackdropFilter(
        filter: ImageFilter.blur(sigmaX: 0.0, sigmaY: 0.0),
        child: Container(
          color: Colors.black.withOpacity(0.1),
          child: child,
        ),
      ),
    );
  }
}
