import 'package:meta/meta.dart';
import 'package:nkey_exchange/app/data/providers/exchange_provider.dart';

class ExchangeRepository {
  final ExchangeApiClient apiClient;

  ExchangeRepository({@required this.apiClient}) : assert(apiClient != null);

  Future<List<String>> getCurrencies() {
    return apiClient.getCurrencies();
  }

  Future<double> getRate(String from, String to) {
    return apiClient.getRate(from, to);
  }
}
