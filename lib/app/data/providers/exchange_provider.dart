import 'package:meta/meta.dart';
import 'package:nkey_exchange/app/utils/dio_nkey.dart';

class ExchangeApiClient {
  final DioNkey httpClient;
  ExchangeApiClient({@required this.httpClient});

  Future<List<String>> getCurrencies() async {
    try {
      var response = await httpClient.get(
        '/latest',
        queryParameters: Map.from({'base': 'BRL'}),
      );

      return response.data['rates'].keys.toList();
    } catch (_) {
      print('${_.request.path} - ${_.message}');
      throw _.message;
    }
  }

  Future<double> getRate(String from, String to) async {
    try {
      var response = await httpClient.get(
        '/latest',
        queryParameters: Map.from({
          'base': from,
          'symbols': to,
        }),
      );

      return response.data['rates'].values.first.toDouble();
    } catch (_) {
      print('${_.request.path} - ${_.message}');
      throw _.message;
    }
  }
}
