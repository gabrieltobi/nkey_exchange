import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:nkey_exchange/app/routes/app_routes.dart';

void main() {
  runApp(GetMaterialApp(
    initialRoute: '/',
    debugShowCheckedModeBanner: false,
    getPages: MyRoutes.routes,
    color: Colors.white,
    title: 'Nkey Exchange',
  ));
}
